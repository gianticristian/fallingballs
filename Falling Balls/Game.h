// Game.h
#pragma once
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
//#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/Music.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include "Player.h"
#include "Balls.h"

using namespace sf;
using namespace std;

class Game {
private:
	RenderWindow *window;
	Player *player;
	Texture border;
	Sprite north;
	Sprite south;
	Sprite east;
	Sprite west;
	bool endGame;					// Para saber si sigue jugando o ya perdio
	// Text
	Font font;						// Fuente para usar en los textos
	Text endText;					// Texto con 'Game Over'
	Clock clock;					// Para controlar el tiempo de juego
	Time timeGame;					// Usa el clock para almacenar el tiempo de juego
	Text timeText;					// Para mostrar el texto con el tiempo
	int timeToShow;					// Convierte y guarda a 'int' el valor de 'timeGame'
	// Musica y Sonidos
	Music music;
	SoundBuffer bufferBalls;
	Sound soundBalls;			// Cuando las bolas chocan con el borde inferior
	SoundBuffer bufferDeath;
	Sound soundDeath;

	// Balls
	Balls *brown;
	Balls *cyan;
	Balls *magenta;
	Balls *red;
	Balls *yellow;
public:
	Game(int width, int height, string title);
	~Game();
	void Go();
	void ProcessEvent(Event &evt);
	void DrawGame();
	void UpdateGame();
	void ProcessCollisions();
};