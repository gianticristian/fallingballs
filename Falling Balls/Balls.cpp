// Balls.cpp
#include "Balls.h"

Balls::Balls(RenderWindow *_window, string path, float scaleX, float scaleY) {
	window = _window;
	texture.loadFromFile(path);
	texture.setSmooth(true);
	sprite.setTexture(texture);
	sprite.setOrigin(texture.getSize().x / 2.0f, texture.getSize().y / 2.0f);
	sprite.setScale(scaleX, scaleY);
	sprite.setPosition(StartPosition());					// Le asigna una posicion inical por arriba de la pantalla para que "caiga"
	SetSpeed();
};

Balls::~Balls() {};

void Balls::DrawThis() {
	window->draw(sprite);
};

void Balls::UpdatePosition() {
	sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y + speed);
};

Sprite Balls::GetSprite() {
	return sprite;
};

void Balls::BorderCollision() {
	// Hacer que si toca el borde de abajo desaparezcan y se muevan un poco mas arriba de la pantalla para volver a aparecer
	sprite.setPosition(StartPosition());
	SetSpeed();
};

Vector2f Balls::StartPosition() {
	Vector2f pos;
	float _x = rand() % (window->getSize().x - 150) + 75.0f;	// Random entre los bordes de la pantalla
	float _y = rand() % 200 - 400.0f;							// Random para ponerlo arriba de todo y asegurar una distancia
	pos.x = _x;
	pos.y = _y;
	return pos;
};

void Balls::SetSpeed() {
	speed = rand() % 5 + 2.0f;								// Le cambia la 'velocidad' en Y
	speed /= 100.0f;										// La divide para suavizarla
};

void Balls::TargetPlayer(float _x) {
	sprite.setPosition(_x, sprite.getPosition().y);
};