// Balls.h
#pragma once
#include <SFML/Graphics.hpp>
#include <string>

using namespace sf;
using namespace std;

class Balls {
private:
	RenderWindow *window;
	Texture texture;
	Sprite sprite;
	float velocityX;
	float velocityY;
	float speed;
	Clock clock;
	float startX;
	float startY;


public:
	Balls(RenderWindow *_window, string path, float scaleX, float scaleY);
	~Balls();
	void DrawThis();
	void UpdatePosition();
	Sprite GetSprite();
	void BorderCollision();
	Vector2f StartPosition();
	void SetSpeed();
	void TargetPlayer(float _x);
};