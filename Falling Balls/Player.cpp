// Player.cpp
#include "Player.h"

Player::Player(RenderWindow *_window, string path, float scaleX, float scaleY, float _speed) {
	window = _window;
	texture.loadFromFile(path);
	texture.setSmooth(true);
	sprite.setTexture(texture);
	sprite.setOrigin(texture.getSize().x / 2.0f, texture.getSize().y / 2.0f);
	sprite.setScale(scaleX, scaleY);
	sprite.setPosition(window->getSize().x / 2.0f, window->getSize().y - 75.0f);
	velocityX = 0.0f;
	velocityY = 0.0f;
	speed = _speed;
};

Player::~Player() {};

void Player::DrawThis() {
	window->draw(sprite);
};

void Player::Move(Keyboard::Key _key) {
	switch (_key) {						// Suma y resta valores en XY para usarlos en 'UpdatePosition'
	case Keyboard::Left:
		velocityX -= speed;
		break;
	case Keyboard::Right:
		velocityX += speed;
		break;
	case Keyboard::Up:
		velocityY -= speed;
		break;
	case Keyboard::Down:
		velocityY += speed;
		break;
	default:
		break;
	}
};

void Player::UpdatePosition() {
	sprite.setPosition(sprite.getPosition().x + velocityX, sprite.getPosition().y + velocityY);
};

Sprite Player::GetSprite() {
	return sprite;
};

void Player::BorderCollision(char _border) {
	switch (_border) {
	case 'Y':
		velocityY *= -1.0f;				// Invierte el sentido de direccion
		break;
	case 'X':
		velocityX *= -1.0f;
	default:
		break;
	}
};

void Player::SetScale() {
	time = clock.getElapsedTime();		// Toma el tiempo transcurrido
	if (time.asSeconds() > 1.0f) {		// Si ya paso un segundo
		sprite.setScale(sprite.getScale().x + 0.0001f, sprite.getScale().y + 0.0001f);	// Aumenta la escala
		clock.restart();				// Reincia el reloj para volver a controlar
	}
};