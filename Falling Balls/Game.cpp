// Game.cpp
#include "Game.h"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

using namespace sf;
using namespace std;

Game::Game(int width, int height, string title) {
	srand(time(NULL));
	// Ventana
	window = new RenderWindow(VideoMode(width, height, 32), title);
	// Bordes del juego
	// Textura
	border.loadFromFile("Images/Border.png");
	border.setSmooth(true);
	// North
	north.setTexture(border);
	north.setScale(0.5f, 1.25f);
	north.setOrigin(border.getSize().x / 2.0f, border.getSize().y / 2.0f);
	north.rotate(90);
	north.setPosition(Vector2f(window->getSize().x / 2.0f, 10.0f));
	// South
	south.setTexture(border);
	south.setScale(0.5f, 1.25f);
	south.setOrigin(border.getSize().x / 2.0f, border.getSize().y / 2.0f);
	south.rotate(90);
	south.setPosition(Vector2f(window->getSize().x / 2.0f, window->getSize().y - 10.0f));
	// East
	east.setTexture(border);
	east.setScale(0.5f, 0.69f);
	east.setOrigin(border.getSize().x / 2.0f, border.getSize().y / 2.0f);
	east.setPosition(Vector2f(10.0f, window->getSize().y / 2.0f));
	// West
	west.setTexture(border);
	west.setScale(0.5f, 0.69f);
	west.setOrigin(border.getSize().x / 2.0f, border.getSize().y / 2.0f);
	west.setPosition(Vector2f(window->getSize().x - 10.0f, window->getSize().y / 2.0f));
	// Player
	player = new Player(window, "Images/Player.png", 0.01f, 0.01f, 0.005f);
	// Balls
	brown = new Balls(window, "Images/Brown.png", 0.015f, 0.015f);
	cyan = new Balls(window, "Images/Cyan.png", 0.015f, 0.015f);
	magenta = new Balls(window, "Images/Magenta.png", 0.015f, 0.015f);
	red = new Balls(window, "Images/Red.png", 0.015f, 0.015f);
	yellow = new Balls(window, "Images/Yellow.png", 0.015f, 0.015f);
	// Variables
	endGame = false;
	// Texto
	if (!font.loadFromFile("Fonts/GROBOLD.ttf")) {
		printf("Can't find the font!\n");
	}
	// Texto | Game Over
	endText.setFont(font);
	endText.setCharacterSize(50);
	endText.setColor(Color::White);
	endText.setPosition(Vector2f(500.0f, 200.0f));			// No hay forma de hacer window->size / 2 y setearlo al origin
	endText.setString("Game Over");
	// Texto | Tiempo de juego
	timeText.setFont(font);
	timeText.setCharacterSize(20);
	timeText.setColor(Color::White);
	timeText.setPosition(Vector2f(1150.0f, 20.0f));
	// Musica
	if (!music.openFromFile("Audio/Melody.ogg")) {
		printf("Can't find the music\n");
	}
	music.setVolume(50);
	music.setLoop(true);
	music.play();
	// Sonidos
	// Bolas chocan el borde sur
	if (!bufferBalls.loadFromFile("Audio/pop1.ogg")) {
		printf("Can't find the sound\n");
	}
	soundBalls.setBuffer(bufferBalls);
	soundBalls.setVolume(60);
	// Player es impactado por una bola y pierde
	if (!bufferDeath.loadFromFile("Audio/Death.wav")) {
		printf("Can't find the sound\n");
	}
	soundDeath.setBuffer(bufferDeath);
	soundDeath.setVolume(60);
};

Game::~Game() {};

void Game::Go() {
	Event evt;
	while (window->isOpen()) {
		while (window->pollEvent(evt)) {
			ProcessEvent(evt);
		}
		ProcessCollisions();
		UpdateGame();
		window->clear();
		DrawGame();
		window->display();
		// Tiempo
		if (!endGame) {										// Mientras en juego
			timeGame = clock.getElapsedTime();				// Va contando los segundos de juego
		}
		else {												// Si pierde
			music.stop();
			timeText.setCharacterSize(30);					// Aumenta el tama�o del texto
			timeText.setPosition(Vector2f(590.0f, 350.0f));	// Lo centra en la pantalla
		}
		timeToShow = (int)timeGame.asSeconds();				// No puedo imprimir el tiempo, entonces lo guardo en una variable 'int'			
		timeText.setString("Time: " + to_string(timeToShow));	// Setea el texto y le asigna el valor de la variable 'int'
	}
};

void Game::ProcessEvent(Event &evt) {
	switch (evt.type) {
		case Event::Closed:									// Si cierra la ventana
			window->close();								// Cierra la aplicacion
			break;
	}
	// Mover el player
	if (Keyboard::isKeyPressed(Keyboard::Left)) {			// Izquierda
		player->Move(Keyboard::Left);
	}
	if (Keyboard::isKeyPressed(Keyboard::Right)) {			// Derecha
		player->Move(Keyboard::Right);
	}
	if (Keyboard::isKeyPressed(Keyboard::Up)) {				// Arriba
		player->Move(Keyboard::Up);
	}
	if (Keyboard::isKeyPressed(Keyboard::Down)) {			// Abajo
		player->Move(Keyboard::Down);
	}
};

void Game::DrawGame() {
	if (!endGame) {											// Si sigue en juego
		// Bordes
		window->draw(north);
		window->draw(south);
		window->draw(east);
		window->draw(west);
		// Player
		player->DrawThis();
		// Bolas
		brown->DrawThis();
		cyan->DrawThis();
		magenta->DrawThis();
		red->DrawThis();
		yellow->DrawThis();
		// Tiempo de juego
		window->draw(timeText);
	}
	else {													// Si perdio
		// Player
		player->DrawThis();
		// Texto
		window->draw(endText);
		window->draw(timeText);
	}
};

void Game::UpdateGame() {
	brown->UpdatePosition();
	cyan->UpdatePosition();
	magenta->UpdatePosition();
	red->UpdatePosition();
	yellow->UpdatePosition();
	player->UpdatePosition();
	player->SetScale();
};

void Game::ProcessCollisions() {
	// Player contra los bordes
	if (player->GetSprite().getGlobalBounds().intersects(north.getGlobalBounds()))	// Toca el borde de arriba
		player->BorderCollision('Y');
	if (player->GetSprite().getGlobalBounds().intersects(south.getGlobalBounds()))	// Toca el borde de arriba
		player->BorderCollision('Y');
	if (player->GetSprite().getGlobalBounds().intersects(east.getGlobalBounds()))	// Toca el borde de la izquierda
		player->BorderCollision('X');
	if (player->GetSprite().getGlobalBounds().intersects(west.getGlobalBounds()))	// Toca el borde de la derecha
		player->BorderCollision('X');
	// Bolas contra el borde de abajo(sur)
	if (brown->GetSprite().getGlobalBounds().intersects(south.getGlobalBounds()) && !endGame) {	// Brown
		brown->BorderCollision();
		soundBalls.play();
	}
	if (cyan->GetSprite().getGlobalBounds().intersects(south.getGlobalBounds()) && !endGame) {	// Cyan
		cyan->BorderCollision();
		soundBalls.play();
	}
	if (magenta->GetSprite().getGlobalBounds().intersects(south.getGlobalBounds()) && !endGame) {	// Magenta
		magenta->BorderCollision();
		soundBalls.play();
	}
	if (red->GetSprite().getGlobalBounds().intersects(south.getGlobalBounds()) && !endGame)	{	// Red
		red->BorderCollision();
		// Hace que la proxima caida de la bola roja sea sobre la posicion del player _
		// para obligarlo a moverse
		red->TargetPlayer(player->GetSprite().getPosition().x);
		soundBalls.play();
	}
	if (yellow->GetSprite().getGlobalBounds().intersects(south.getGlobalBounds()) && !endGame) {	// Yellow
		yellow->BorderCollision();
		// Obtiene la posicion del player en 'x'
		yellow->TargetPlayer(player->GetSprite().getPosition().x);
		soundBalls.play();
	}
	// Player contra bolas
	if (player->GetSprite().getGlobalBounds().intersects(brown->GetSprite().getGlobalBounds()) && !endGame) {
		soundDeath.play();
		endGame = true;
	}
	if (player->GetSprite().getGlobalBounds().intersects(cyan->GetSprite().getGlobalBounds()) && !endGame) {
		soundDeath.play();
		endGame = true;
	}
	if (player->GetSprite().getGlobalBounds().intersects(magenta->GetSprite().getGlobalBounds()) && !endGame) {
		soundDeath.play();
		endGame = true;
	}
	if (player->GetSprite().getGlobalBounds().intersects(red->GetSprite().getGlobalBounds()) && !endGame) {
		soundDeath.play();
		endGame = true;
	}
	if (player->GetSprite().getGlobalBounds().intersects(yellow->GetSprite().getGlobalBounds()) && !endGame) {
		soundDeath.play();
		endGame = true;
	}

};
