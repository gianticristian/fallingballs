// Main.cpp
#include "Game.h"

using namespace sf;
using namespace std;

int main() {
	Game game(1280, 720, "Falling Balls");		// Inicia el juego
	game.Go();									// Se juega
	//game.~Game();								// Se cierra y libera la ram
	return 0;
};