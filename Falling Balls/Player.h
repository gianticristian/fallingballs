// Player.h
#pragma once
#include <SFML/Graphics.hpp>
#include <string>

using namespace sf;
using namespace std;

class Player {
private:
	RenderWindow *window;
	Texture texture;
	Sprite sprite;
	float velocityX;
	float velocityY;
	float speed;
	Clock clock;				// Para ir usando 'SetScale'
	Time time;					// Usa el clock para 'SetScale'


public:
	Player(RenderWindow *_window, string path, float scaleX, float scaleY, float _speed);
	~Player();
	void DrawThis();
	void Move(Keyboard::Key _key);
	void UpdatePosition();
	Sprite GetSprite();
	void BorderCollision(char _border);
	void SetScale();
};